#include "single.h"


Single Single::single;
Single& Single::install()
{
    return single;
}

Single::Single(QObject *parent) : QObject(parent)
{

}

Mode Single::getMode() const
{
    return mode;
}

void Single::setMode(const Mode &value)
{
    mode = value;
}
