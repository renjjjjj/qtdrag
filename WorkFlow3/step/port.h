#ifndef PORT_H
#define PORT_H

#include <QGraphicsItem>
#include <QObject>
class CPort;
class Port : public QObject,public QGraphicsItem
{
    Q_OBJECT

public:
    explicit Port(QObject* parent = nullptr);
    ~Port();
    enum { Type = UserType+1};
    QRectF boundingRect() const override;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;


    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

public:
    int type() const override { return Type; }

    int getInside_raduis() const;
    void setInside_raduis(int inside_raduis);

    int getOutside_raduis() const;
    void setOutside_raduis(int outside_raduis);


    CPort* getCport();
    void setCport(CPort *value);

private:
    int  m_inside_raduis =5;
    int  m_outside_raduis =20;
    bool is_hover= false;

    CPort *cport=nullptr;


};

#endif // PORT_H
