#ifndef BACKDEALSTEP_H
#define BACKDEALSTEP_H

#include <QObject>
#include "stepbase.h"
class BackDealStep:public StepBase
{
    Q_OBJECT
public:
    explicit BackDealStep(StepBase* parent=nullptr);
    ~BackDealStep();

    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:
    int raduis = 20;

};
#endif // BACKDEALSTEP_H
