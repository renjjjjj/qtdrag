HEADERS += \
    $$PWD/backdealstep.h \
    $$PWD/geometrystep.h \
    $$PWD/gridstep.h \
    $$PWD/conditionstep.h \
    $$PWD/diagramtextitem.h \
    $$PWD/endstep.h \
    $$PWD/loopstep.h \
    $$PWD/port.h \
    $$PWD/relation.h \
    $$PWD/soverstep.h \
    $$PWD/startstep.h \
    $$PWD/stepbase.h \

SOURCES += \
    $$PWD/backdealstep.cpp \
    $$PWD/geometrystep.cpp \
    $$PWD/gridstep.cpp \
    $$PWD/conditionstep.cpp \
    $$PWD/diagramtextitem.cpp \
    $$PWD/endstep.cpp \
    $$PWD/loopstep.cpp \
    $$PWD/port.cpp \
    $$PWD/relation.cpp \
    $$PWD/soverstep.cpp \
    $$PWD/startstep.cpp \
    $$PWD/stepbase.cpp \
