#include "geometrystep.h"
#include <QPainter>
#include <QGraphicsSceneDragDropEvent>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <QPen>
#include <QStyle>
#include <QGraphicsLineItem>
#include <QDebug>
#include "single.h"

GeometryStep::GeometryStep(StepBase* parent):StepBase(parent)
{

    //textItem->setPlainText("通用开始组件");
    init();

}
GeometryStep::~GeometryStep()
{

}

QRectF GeometryStep::boundingRect() const
{
    return QRectF(-m_width/2, -m_height/2, m_width, m_height);
}

void GeometryStep::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);


    QList<QGraphicsItem *> items = scene()->collidingItems(this);
    if((option->state & QStyle::State_Selected)||(!items.isEmpty()&&items.first()->type()==6))
    {
        const qreal penWidth = 2;
        // 边框区域颜色
        QColor color = QColor(Qt::blue);
        painter->setPen(QPen(color, penWidth, Qt::DashLine));
    }
    painter->setBrush(Qt::red);
    painter->drawRoundedRect(boundingRect(), raduis, raduis);



}
