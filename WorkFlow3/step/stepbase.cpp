#include "stepbase.h"
#include "single.h"
#include "workflowxml.h"
StepBase::StepBase(QObject* parent ):QObject(parent)
{

}

StepBase::~StepBase()
{
    qDebug()<<__func__;

}

CPort* StepBase::createPort(CPort* _cport)
{
    int count = ports.count();
    Port* port = nullptr;
    const qreal width = m_width / 2;
    const qreal height = m_height / 2;
    CPortProperty::LOCATION location;

    port = new Port;
    port->setParentItem(this);
    if(_cport == nullptr)
    {
        switch(count)
        {
        case 0:
            location = CPortProperty::RIGHT;
            port->setPos(mapToItem(this,width,0));
            break;
        case 1:
            location = CPortProperty::LEFT;
            port->setPos(mapToItem(this,-width,0));
            break;
        case 2:
            location = CPortProperty::TOP;
            port->setPos(mapToItem(this,0,-height));
            break;
        case 3:
            location = CPortProperty::BOTTOM;
            port->setPos(mapToItem(this,0,height));
            break;
        }

        if(port!=nullptr)
        {
            CPortProperty* _property = new CPortProperty{ this->cstep->getProperty()->name +"_port_" + QString::number(count+1),"xxx","in",location};
            CPort* cport = new CPort(_property);
            port->setCport(cport);
            ports.push_back(port);
            return cport;
        }
    }
    else
    {
        switch(_cport->getProperty()->location)
        {
        case CPortProperty::RIGHT:
            port->setPos(mapToItem(this,width,0));
            break;
        case CPortProperty::LEFT:
            port->setPos(mapToItem(this,-width,0));
            break;
        case CPortProperty::TOP:
            port->setPos(mapToItem(this,0,-height));
            break;
        case CPortProperty::BOTTOM:
            port->setPos(mapToItem(this,0,height));
            break;
        }
        port->setCport(_cport);
        ports.push_back(port);
    }
    return nullptr;

}

void StepBase::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{

    update();
    if(Single::install().getMode() == InsertLine)
    {
        setFlag(QGraphicsItem::ItemIsMovable, false);
    }
    return QGraphicsItem::hoverEnterEvent(event);
}

void StepBase::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{

    update();
    if(Single::install().getMode() == InsertLine)
    {
        setFlag(QGraphicsItem::ItemIsMovable, true);
    }
    return QGraphicsItem::hoverLeaveEvent(event);
}


QVariant StepBase::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionChange)
    {
        for (Port *port : qAsConst(ports))
        {

            CPort* _cport = port->getCport();
            if(_cport)
            {
                _cport->setPos(port->scenePos());
                emit _cport->posChange();
            }
        }
        if(cstep)
        {
            cstep->setPos(scenePos());
        }
    }
    return value;
}

void StepBase::init()
{
    //设置flag
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
    setAcceptHoverEvents(true);


    //添加组件名
    textItem= new DiagramTextItem;
    textItem->document()->setTextWidth(m_width);
    QTextOption option = textItem->document()->defaultTextOption();//取出当前设置
    option.setAlignment(Qt::AlignHCenter);//对设置进行修改，增加居中对齐设置
    textItem->document()->setDefaultTextOption(option);//重新设定
    textItem->setParentItem(this);
    textItem->setPos(mapToItem(this,-m_width/2,m_height/2));
    textItem->setPlainText("通用组件");
    //添加信号槽
    if(cstep)
    {
        connect(cstep,&CStep::destroyed,this,[=]()
        {
            scene()->removeItem(this);
            delete this;
        });
    }

}

CStep *StepBase::getCstep() const
{
    return cstep;
}

void StepBase::setCstep(CStep *value)
{
    if(cstep)
    {
        disconnect(cstep);
    }
    textItem->setPlainText(value->getProperty()->name);
    cstep = value;

    //添加信号槽
    connect(cstep,&CStep::destroyed,this,[=]()
    {
        scene()->removeItem(this);
        delete this;
    });
}
