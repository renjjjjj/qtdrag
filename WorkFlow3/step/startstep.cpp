#include "startstep.h"
#include <QPainter>
#include <QGraphicsSceneDragDropEvent>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <QPen>
#include <QStyle>
#include <QGraphicsLineItem>
#include <QDebug>
#include "single.h"

StartStep::StartStep(StepBase* parent):StepBase(parent)
{
    //textItem->setPlainText("通用开始组件");
    init();
}
StartStep::~StartStep()
{
    qDebug()<<__func__;
}

QRectF StartStep::boundingRect() const
{
    return QRectF(-m_width/2, -m_height/2, m_width, m_height);
}

void StartStep::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);


    QList<QGraphicsItem *> items = scene()->collidingItems(this);
    if((option->state & QStyle::State_Selected)||(!items.isEmpty()&&items.first()->type()==6))
    {
        const qreal penWidth = 2;
        // 边框区域颜色
        QColor color = QColor(Qt::blue);
        painter->setPen(QPen(color, penWidth, Qt::DashLine));
    }
    painter->setBrush(Qt::red);
    painter->drawRoundedRect(boundingRect(), raduis, raduis);



}

CPort* StartStep::createPort(CPort* _cport)
{
    int count = ports.count();
    Port* port = nullptr;
    const qreal height = m_height / 2;
    if(count == 0)
    {
        port = new Port;
        port->setParentItem(this);
        port->setPos(mapToItem(this,0,height));
        //scene()->addItem(port);
    }

    if(port!=nullptr)
    {
        if(_cport ==nullptr)
        {
            CPortProperty* _property = new CPortProperty{this->cstep->getProperty()->name +"_port_" + QString::number(count+1),"xxx","in",CPortProperty::TOP};
            CPort* cport = new CPort(_property);
            port->setCport(cport);
            ports.push_back(port);
            return cport;
        }
        else
        {
            port->setCport(_cport);
        }


    }
    return nullptr;

}


