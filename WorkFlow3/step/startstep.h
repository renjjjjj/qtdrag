#ifndef STARTSTEP_H
#define STARTSTEP_H

#include "stepbase.h"
class StartStep:public StepBase
{
    Q_OBJECT
public:
    explicit StartStep(StepBase* parent=nullptr);
    ~StartStep();

    virtual CPort* createPort(CPort* _cport = nullptr) override;
    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:

    int raduis = 20;

};

#endif // STARTSTEP_H
