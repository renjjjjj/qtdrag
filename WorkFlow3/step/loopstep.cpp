#include "loopstep.h"
#include <QPainter>
#include <QGraphicsSceneDragDropEvent>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <QPen>
#include <QStyle>
#include <QGraphicsLineItem>
#include <QDebug>
#include "single.h"

LoopStep::LoopStep(StepBase* parent):StepBase(parent)
{

//    m_width = 150;
//    m_height = 80;
    init();
//    int width = m_width/2;
//    int heigt = m_height/2;
//    myPolygon<< QPointF(-width, 0) << QPointF(0, heigt)
//               << QPointF(width, 0) << QPointF(0, -heigt)
//              << QPointF(-width, 0);

}
LoopStep::~LoopStep()
{

}

QRectF LoopStep::boundingRect() const
{
    return QRectF(-m_width/2, -m_height/2, m_width, m_height);
}

void LoopStep::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);


    QList<QGraphicsItem *> items = scene()->collidingItems(this);
    if((option->state & QStyle::State_Selected)||(!items.isEmpty()&&items.first()->type()==6))
    {
        const qreal penWidth = 2;
        // 边框区域颜色
        QColor color = QColor(Qt::blue);
        painter->setPen(QPen(color, penWidth, Qt::DashLine));
    }
    painter->setBrush(Qt::red);
    painter->drawRoundedRect(boundingRect(), raduis, raduis);
    //painter->drawPolygon(myPolygon);



}


