#ifndef LOOPSTEP_H
#define LOOPSTEP_H

#include "stepbase.h"
class LoopStep:public StepBase
{
    Q_OBJECT
public:
    explicit LoopStep(StepBase* parent=nullptr);
    ~LoopStep();

    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:
    int raduis = 20;
    //QPolygonF myPolygon;
};
#endif // LOOPSTEP_H
