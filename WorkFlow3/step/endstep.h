#ifndef ENDSTEP_H
#define ENDSTEP_H

#include <QObject>

#include "stepbase.h"
class EndStep:public StepBase
{
    Q_OBJECT
public:
    explicit EndStep(StepBase* parent=nullptr);
    ~EndStep();

    virtual CPort* createPort(CPort* _cport = nullptr) override;
    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:

    int raduis = 20;

};
#endif // ENDSTEP_H
