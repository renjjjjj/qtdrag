#ifndef GRIDSTEP_H
#define GRIDSTEP_H

#include <QWidget>

#include <QObject>
#include "stepbase.h"
class GridStep:public StepBase
{
    Q_OBJECT
public:
    explicit GridStep(StepBase* parent=nullptr);
    ~GridStep();

    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:

    int raduis = 20;

};
#endif // GRIDSTEP_H
