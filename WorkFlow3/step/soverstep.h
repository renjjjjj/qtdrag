#ifndef SOVERSTEP_H
#define SOVERSTEP_H

#include <QObject>
#include "stepbase.h"
class SoverStep:public StepBase
{
    Q_OBJECT
public:
    explicit SoverStep(StepBase* parent=nullptr);
    ~SoverStep();

    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:

    int raduis = 20;

};
#endif // SOVERSTEP_H
