#include "port.h"
#include "single.h"
#include <QPainter>
#include <QGraphicsScene>
#include "workflowxml.h"
Port::Port(QObject* parent):QObject(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable, false);
    setFlag(QGraphicsItem::ItemIsSelectable, false);
    //setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
    setAcceptHoverEvents(true);
}

Port::~Port()
{
    qDebug()<<__func__;
}
QRectF Port::boundingRect() const
{
    return QRectF(-m_outside_raduis, -m_outside_raduis, m_outside_raduis*2, m_outside_raduis*2);
}

void Port::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(QPen(Qt::blue,1,Qt::SolidLine));
    painter->setBrush(Qt::white);

    QList<QGraphicsItem*> list = scene()->collidingItems(this);
    if(is_hover)
    {
        painter->setBrush(Qt::red);
    }
    else
    {
        for(auto item:list)
        {
            if(item->type() == 6)
            {
                painter->setBrush(Qt::red);
                break;
            }
        }
    }
    painter->drawEllipse(-m_inside_raduis,-m_inside_raduis,m_inside_raduis*2,m_inside_raduis*2);

}

void Port::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if(Single::install().getMode() == InsertLine)
    {
        is_hover = true;
        update();
    }
    return QGraphicsItem::hoverEnterEvent(event);
}

void Port::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{

    if(Single::install().getMode() == InsertLine)
    {
        is_hover = false;
        update();
    }
    return QGraphicsItem::hoverLeaveEvent(event);
}


int Port::getInside_raduis() const
{
    return m_inside_raduis;
}

void Port::setInside_raduis(int inside_raduis)
{
    m_inside_raduis = inside_raduis;
}

int Port::getOutside_raduis() const
{
    return m_outside_raduis;
}

void Port::setOutside_raduis(int outside_raduis)
{
    m_outside_raduis = outside_raduis;
}


CPort* Port::getCport()
{
    return cport;
}


void Port::setCport(CPort *value)
{
    cport = value;
    connect(cport,&CPort::updatePos,this,[=]()
    {
        cport->setPos(this->scenePos());
        emit cport->posChange();
    });
}
