#ifndef GEOMETRYSTEP_H
#define GEOMETRYSTEP_H

#include <QObject>
#include "stepbase.h"
class GeometryStep:public StepBase
{
    Q_OBJECT
public:
    explicit GeometryStep(StepBase* parent=nullptr);
    ~GeometryStep();

    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:

    int raduis = 20;

};
#endif // GEOMETRYSTEP_H
