#ifndef CONDITIONSTEP_H
#define CONDITIONSTEP_H

#include <QObject>

#include "stepbase.h"
class ConditionStep:public StepBase
{
    Q_OBJECT
public:
    explicit ConditionStep(StepBase* parent=nullptr);
    ~ConditionStep();

    //int type() const override { return  WorkFlowXML::Start; }
private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=nullptr) override;
private:

    QPolygonF myPolygon;
};
#endif // CONDITIONSTEP_H
