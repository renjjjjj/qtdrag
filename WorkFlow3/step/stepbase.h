#ifndef STEPBASE_H
#define STEPBASE_H

#include <QObject>
#include <QGraphicsItem>
#include "diagramtextitem.h"
#include "port.h"
class CStep;

class StepBase : public QObject,public QGraphicsItem
{
    Q_OBJECT
public:
    explicit StepBase(QObject *parent = nullptr);
    virtual ~StepBase();


public:
    virtual CPort* createPort(CPort* _cport = nullptr);

    CStep *getCstep() const;
    void setCstep(CStep *value);

protected:
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
    virtual void init();

protected:
    DiagramTextItem* textItem;
    QVector<Port*> ports;
    int m_width=120;
    int m_height=60;
    CStep* cstep=nullptr;

};

#endif // STEPBASE_H
