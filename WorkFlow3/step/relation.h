#ifndef RELATION_H
#define RELATION_H
#include <QGraphicsLineItem>
#include <QGraphicsItem>
class Port;
class CRelation;
class Relation : public QObject,public QGraphicsLineItem
{
    Q_OBJECT
public:
    enum { Type = UserType };
    Relation(CRelation* _relation,QObject* parent=nullptr);
    ~Relation();
    int type() const override { return Type; }
    QRectF boundingRect() const override;
    void setColor(const QColor &color) { myColor = color; }

    void updatePosition();


    CRelation *getCrelation() const;
    void setCrelation(CRelation *value);

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    QPainterPath  shape() const override;


private:;
    void paintPolygon(QPainter *painter);

private:
    CRelation* crelation=nullptr;
    QColor myColor = Qt::black;
    int width = 5;
    int lineLen = 20; //需要转折的引出长度
    QPolygonF myPolygon;


};

#endif // RELATION_H
