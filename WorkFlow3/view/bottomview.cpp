#include "bottomview.h"
#include "ui_bottomview.h"

BottomView::BottomView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BottomView)
{
    ui->setupUi(this);
}

BottomView::~BottomView()
{
    delete ui;
}
QSize BottomView::sizeHint() const
{
    return QSize(1200,200);
}
