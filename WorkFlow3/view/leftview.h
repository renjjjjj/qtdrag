#ifndef LEFTVIEW_H
#define LEFTVIEW_H

#include <QWidget>

namespace Ui {
class LeftView;
}

class QStandardItemModel;
class QStandardItem;
class LeftView : public QWidget
{
    Q_OBJECT

public:
    explicit LeftView(QWidget *parent = nullptr);
    ~LeftView();

    virtual QSize sizeHint() const;
    QStandardItemModel *getFlowModel() const;
    void setFlowModel(QStandardItemModel *value);

private slots:
    void on_viewFlow_clicked(const QModelIndex &index);

private:
    void LoadGengeralStep();
    void InitForm();
signals:
    void WorkFlowChange(const QModelIndex &index);
    void WorkFlowNameChange(const QModelIndex &index);
private:
    Ui::LeftView *ui;

    QStandardItemModel* gengeralModel = nullptr;
    QStandardItemModel* slefModel = nullptr;
    QStandardItemModel* flowModel = nullptr;
    QStandardItem* geogetryTreeItem;
    QStandardItem* gridTreeItem;
    QStandardItem* soverTreeItem;
    QStandardItem* backDealTreeItem;

};

#endif // LEFTVIEW_H
