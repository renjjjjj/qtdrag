#ifndef RIGHTVIEW_H
#define RIGHTVIEW_H

#include <QWidget>

namespace Ui {
class RightView;
}

class RightView : public QWidget
{
    Q_OBJECT

public:
    explicit RightView(QWidget *parent = nullptr);
    ~RightView();
    virtual QSize sizeHint() const;
    void initForm();

private:
    Ui::RightView *ui;
};

#endif // RIGHTVIEW_H
