#include "rightview.h"
#include "ui_rightview.h"

RightView::RightView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RightView)
{
    ui->setupUi(this);
}

RightView::~RightView()
{
    delete ui;
}

QSize RightView::sizeHint() const
{
    return QSize(240,600);
}

void RightView::initForm()
{

}
