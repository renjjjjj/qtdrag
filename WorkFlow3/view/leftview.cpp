#include "leftview.h"
#include "ui_leftview.h"
#include "workflowxml.h"
#include <QStandardItemModel>

LeftView::LeftView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LeftView)
{
    ui->setupUi(this);
    InitForm();
    LoadGengeralStep();

}

LeftView::~LeftView()
{
    delete ui;
}

QSize LeftView::sizeHint() const
{
    return QSize(240,600);
}

void LeftView::InitForm()
{
    gengeralModel = new QStandardItemModel(this);
    ui->viewGeneral->setModel(gengeralModel);

    slefModel = new QStandardItemModel(this);
    ui->viewSelf->setModel(slefModel);

    flowModel = new QStandardItemModel(this);
    ui->viewFlow->setModel(flowModel);

    geogetryTreeItem = new QStandardItem(QPixmap(""),"几何类") ;
    gridTreeItem     = new QStandardItem(QPixmap(""),"网格类") ;
    soverTreeItem    = new QStandardItem(QPixmap(""),"求解器类");
    backDealTreeItem = new QStandardItem(QIcon(""),"后处理类");


    slefModel->appendRow(geogetryTreeItem );
    slefModel->appendRow(gridTreeItem     );
    slefModel->appendRow(soverTreeItem   );
    slefModel->appendRow(backDealTreeItem);

    ui->viewSelf->setHeaderHidden(true);
    ui->viewSelf->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->viewGeneral->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(flowModel,&QStandardItemModel::dataChanged,this,&LeftView::WorkFlowNameChange);

}

QStandardItemModel *LeftView::getFlowModel() const
{
    return flowModel;
}

void LeftView::setFlowModel(QStandardItemModel *value)
{
    flowModel = value;
}


void LeftView::LoadGengeralStep()
{
    QStringList listComponents;
    listComponents << "开始组件" << "结束组件" << "循环组件" << "条件判断组件" << "通用几何组件"
                   << "通用网格组件" << "通用求解器组件" << "通用后处理组件";

    QStringList listIcons;
    listIcons << ":/images/startstep.png" << ":/images/endstep.png" << ":/images/recyclestep.png" << ":/images/conditionstep.png"
                 << ":/images/geostep.png" << ":/images/meshstep.png" << ":/images/solverstep.png" << ":/images/poststep.png";

    int index=Start;
    for (int i=0; i<listComponents.size(); i++)
    {

        QStandardItem *pitem = new QStandardItem();
        pitem->setText(listComponents.at(i));
        pitem->setIcon(QIcon(listIcons.at(i)));
        pitem->setData(QVariant(index+i),Qt::UserRole+1);
        gengeralModel->appendRow(pitem);
    }
}

void LeftView::on_viewFlow_clicked(const QModelIndex &index)
{
    emit WorkFlowChange(index);
}
