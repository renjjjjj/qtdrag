FORMS += \
    $$PWD/bottomview.ui \
    $$PWD/leftview.ui \
    $$PWD/rightview.ui

HEADERS += \
    $$PWD/bottomview.h \
    $$PWD/centreview.h \
    $$PWD/leftview.h \
    $$PWD/listview.h \
    $$PWD/rightview.h \
    $$PWD/thumbnail.h \
    $$PWD/treeview.h

SOURCES += \
    $$PWD/bottomview.cpp \
    $$PWD/centreview.cpp \
    $$PWD/leftview.cpp \
    $$PWD/listview.cpp \
    $$PWD/rightview.cpp \
    $$PWD/thumbnail.cpp \
    $$PWD/treeview.cpp
