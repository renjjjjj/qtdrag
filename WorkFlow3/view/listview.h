#ifndef LISTVIEW_H
#define LISTVIEW_H

#include <QListView>

class ListView : public QListView
{
    Q_OBJECT
public:
    explicit ListView(QWidget *parent = nullptr);


    //int offset() const {return 19;}
    int dragRow() const {return theDragRow;}
    static QString myMimeType() { return QStringLiteral("dragramView/text-icon-icon_hover"); }

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dropEvent(QDropEvent *event) override;
private:
    QPoint startPos;
    int theDragRow = -1;

};

#endif // LISTVIEW_H
