#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <QTreeView>

class TreeView : public QTreeView
{
    Q_OBJECT
public:
    explicit TreeView(QWidget *parent = nullptr);

    static QString myMimeType() { return QStringLiteral("dragramView/text-icon-icon_hover"); }


protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    //void dropEvent(QDropEvent *event);

private:
    QPoint startPos;
    QModelIndex pressIndex;

};

#endif // TREEVIEW_H
