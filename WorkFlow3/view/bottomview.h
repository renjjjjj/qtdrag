#ifndef BOTTOMVIEW_H
#define BOTTOMVIEW_H

#include <QWidget>

namespace Ui {
class BottomView;
}

class BottomView : public QWidget
{
    Q_OBJECT

public:
    explicit BottomView(QWidget *parent = nullptr);
    ~BottomView();

    QSize sizeHint() const;
private:
    Ui::BottomView *ui;
};

#endif // BOTTOM_H
