#ifndef SINGLE_H
#define SINGLE_H

#include <QObject>
#include "workflowxml.h"
class Single : public QObject
{
    Q_OBJECT
public:
    static Single& install();

    Mode getMode() const;
    void setMode(const Mode &value);

private:
    static Single single;
    explicit Single(QObject *parent = nullptr);
    Mode mode;

};

#endif // SINGLE_H
