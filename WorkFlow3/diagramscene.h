#ifndef DIAGRAMSCENE_H
#define DIAGRAMSCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsSceneDragDropEvent>

class StepBase;
class WorkFlow;
class CStep;
class DiagramScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit DiagramScene(QObject *parent = nullptr);

    WorkFlow *getWorkFlow() const;
    void setWorkFlow(WorkFlow *value);
    void openWorkFlow(WorkFlow *_workFlow);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

    static QString myMimeType() { return QStringLiteral("dragramView/text-icon-icon_hover"); }
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dropEvent(QGraphicsSceneDragDropEvent *event) override;

private:
    StepBase *createStep(int _type);
    StepBase* createStartStep(CStep* cstep = nullptr);
    StepBase* createEndStep(CStep* cstep = nullptr);
    StepBase *createLoopStep(CStep* cstep = nullptr);
    StepBase* createConditionStep(CStep* cstep = nullptr);
    StepBase* createGeometryStep(CStep* cstep = nullptr);
    StepBase* createGridStep(CStep* cstep = nullptr);
    StepBase* createSoverStep(CStep* cstep = nullptr);
    StepBase* createBackDealStep(CStep* cstep = nullptr);

private:
    QGraphicsLineItem *line = nullptr;

    int startCount= 0;
    int endCount =0;
    int loopCount = 0;
    int conditionCount = 0;
    int geometryCount = 0;
    int gridCount = 0;
    int soverCount =0;
    int backDeakCount = 0;
    int relationCount = 0;
    int id=0;


    WorkFlow* workFlow = nullptr;





};

#endif // DIAGRAMSCENE_H
