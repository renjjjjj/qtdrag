#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QToolButton;
class QButtonGroup;
class WorkFlow;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class CentreView;
class DiagramScene;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    void initWindow();
    void initToolBar();
    void initStatusBar();
    void initMenuBar();


    void createWorkFlowTriggered();
    void pointerGroupClicked(int id);
    void delectStepTriggered();
    void openWorkFlowTriggered();



public:

private:
    Ui::MainWindow *ui;
    CentreView *contreView = nullptr;
    DiagramScene* currentScene = nullptr;
    QToolButton *pointerButton;
    QToolButton *linePointerButton;
    QButtonGroup* pointerTypeGroup;



};
#endif // MAINWINDOW_H
