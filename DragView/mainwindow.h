#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class DiagramScene;
class StepBase;
class ShadowFrame;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum FLOW_TYPE
    {
        INVALID,
        FLOW1,
        FLOW2,
        FLOW3,
        FLOW4,

    };
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initWindow();
    void initToolBar();
    void initView();
    void deleteItem();
    void resetcentreView();
    void ShowDialog(StepBase* _step);

    void checkFlow();

    void showShadowImpl();              //  对整个界面用阴影进行遮挡
    void hideShadowImpl();
private:
    Ui::MainWindow *ui;
    DiagramScene* currentScene = nullptr;
    ShadowFrame* m_pShadowFrame = nullptr;
    FLOW_TYPE flowType = INVALID;
};
#endif // MAINWINDOW_H
