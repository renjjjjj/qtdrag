#ifndef DIAGRAMSCENE_H
#define DIAGRAMSCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsSceneDragDropEvent>

class Relation;
class StepBase;
class StepData;
class DiagramScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit DiagramScene(QObject *parent = nullptr);

    void resetScene();
    void collidingandRelation(StepBase* _step);   //和线条碰撞的检测
    void ComponentOrdering( QList<StepBase*>& list);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    static QString myMimeType() { return QStringLiteral("dragramView/text-icon-icon_hover"); }
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dropEvent(QGraphicsSceneDragDropEvent *event) override;
private :
    void  slotsDoubleClickedItem();
    StepBase* createStep(const StepData& _data);
    bool findType(int type);

signals:
    void doubleClickedItem(StepBase*);
private:
    void initForm();
    void createRelation();
    void initStep();

private:
    QGraphicsLineItem *line = nullptr;

    std::array<Relation*,4> relation = {0};





};



#endif // DIAGRAMSCENE_H
