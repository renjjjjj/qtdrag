#include "stepbase.h"
#include <QDebug>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include "diagramscene.h"
StepBase::StepBase(QObject* parent ):QObject(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

StepBase::~StepBase()
{
    qDebug()<<__func__;

}

QRectF StepBase::boundingRect() const
{
    return QRectF(-m_width/2, -m_height/2, m_width, m_height);
}

void StepBase::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::gray);
    const qreal penWidth = 2;

    if(option->state & QStyle::State_Selected)
    {
        QColor color = QColor(Qt::blue);
        painter->setPen(QPen(color, penWidth, Qt::DashLine));

    }
    else
    {
        QColor color = QColor(Qt::gray);
        painter->setPen(QPen(color, penWidth, Qt::SolidLine));
    }
    painter->drawRoundedRect(boundingRect(), raduis, raduis);


    QRectF iconRect = QRectF(boundingRect().adjusted(20,10,-20,-30));
    QRectF textRect = QRect(boundingRect().x(),boundingRect().y()+70,boundingRect().width(),30);

    painter->drawPixmap(iconRect.x(),iconRect.y(), m_data.m_pix);


    painter->setPen(QPen(Qt::black));
    painter->setFont(QFont("Microsoft Yahei", 10));
    painter->drawText(textRect, Qt::AlignCenter,m_data.m_text);

}

void StepBase::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    emit doubleClicked();
    QGraphicsItem::mouseDoubleClickEvent(event);
}

void StepBase::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    DiagramScene* _scene = dynamic_cast<DiagramScene*>(scene());
    if(_scene!=nullptr)
    {
        _scene->collidingandRelation(this);
    }
    QGraphicsItem::mouseReleaseEvent(event);
}

Relation::DIRECTION StepBase::direction() const
{
    return m_direction;
}

void StepBase::setDirection(const Relation::DIRECTION &direction)
{
    m_direction = direction;
}




StepData StepBase::data() const
{
    return m_data;
}

void StepBase::setData(const StepData &data)
{
    m_data = data;
    update();
}





