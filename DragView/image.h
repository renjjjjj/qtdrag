#ifndef IMAGE_H
#define IMAGE_H
#define EXCHANGE_IMAGE ":/images/exchange.png"
#define STRUCTURE_IMAGE ":/images/structure.png"

#define MESH_IMAGE ":/images/mesh.png"
#define GEOMETRY_IMAGE ":/images/geometry.png"
#define OPTIMIZE_IMAGE ":/images/optimize.png"
#define SAMPLING_IMAGE ":/images/sampling.png"


#endif // IMAGE_H
