#ifndef STEPBASE_H
#define STEPBASE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPointer>
#include <QVariant>
#include "relation.h"
class StepData{
public:
    enum STEP_TYPE{EXCHANGE = QGraphicsItem::UserType+2,STRUCTURE,MESH,GEOMETRY,OPTIMIZE,SAMPLING};
    StepData(){}
    StepData(QPixmap _pix ,QString _text ,STEP_TYPE _type)
        :m_pix(_pix),m_text(_text),m_stepType(_type)
    {

    }
    QPixmap m_pix;
    QString m_text;
    STEP_TYPE m_stepType = EXCHANGE;
};
Q_DECLARE_METATYPE (StepData)

class StepBase : public QObject,public QGraphicsItem
{
    Q_OBJECT
public:
    explicit StepBase(QObject *parent = nullptr);
    virtual ~StepBase();

    int type() const override { return m_data.m_stepType; }
    StepData data() const;
    void setData(const StepData &data);



    Relation::DIRECTION direction() const;
    void setDirection(const Relation::DIRECTION &direction);

protected:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

signals:
    void doubleClicked();
protected:
    int m_width=100;
    int m_height=100;
    int raduis = 10;

    StepData m_data;
    Relation::DIRECTION m_direction= Relation::NODIRECTION;




};

#endif // STEPBASE_H
