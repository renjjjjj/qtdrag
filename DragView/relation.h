#ifndef RELATION_H
#define RELATION_H
#include <QGraphicsLineItem>
#include <QGraphicsItem>

class Relation : public QObject,public QGraphicsLineItem
{
    Q_OBJECT
public:
    enum { Type = UserType+1 };
    enum DIRECTION{NODIRECTION,UP,DOWN,LEFT,RIGHT};

    Relation(QObject* parent=nullptr);
    ~Relation();
    int type() const override { return Type; }
    QRectF boundingRect() const override;
    void setColor(const QColor &color) { myColor = color; }

    void updatePosition();
    void addArrow(int _direction);
    DIRECTION direction() const;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    QPainterPath  shape() const override;



private:
    QColor myColor = Qt::black;
    int width = 5;
    int lineLen = 20; //需要转折的引出长度
    int arrowWidth = 10;
    QPolygonF myPolygon;
    DIRECTION m_direction= NODIRECTION;


};

#endif // RELATION_H
