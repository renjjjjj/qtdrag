#ifndef LEFTVIEW_H
#define LEFTVIEW_H

#include <QWidget>

namespace Ui {
class LeftView;
}

class QStandardItemModel;
class QStandardItem;
class LeftView : public QWidget
{
    Q_OBJECT

public:
    explicit LeftView(QWidget *parent = nullptr);
    ~LeftView();

    virtual QSize sizeHint() const;
private:
    void LoadGengeralStep();
    void InitForm();
private:
    Ui::LeftView *ui;

    QStandardItemModel* gengeralModel = nullptr;

};

#endif // LEFTVIEW_H
