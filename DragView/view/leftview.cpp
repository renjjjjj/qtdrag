#include "leftview.h"
#include "ui_leftview.h"
#include <QStandardItemModel>
#include <QVariant>
#include <QStandardItem>
#include "delegate.h"
#include <QBoxLayout>
#include "stepbase.h"
#include "image.h"
#include <QDebug>
LeftView::LeftView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LeftView)
{
    ui->setupUi(this);
    InitForm();
    LoadGengeralStep();



}

LeftView::~LeftView()
{
    delete ui;
}

QSize LeftView::sizeHint() const
{
    return QSize(200,600);
}

void LeftView::InitForm()
{
    gengeralModel = new QStandardItemModel(this);
    ui->listView->setModel(gengeralModel);
    Delegate* delegate = new Delegate(ui->listView);
    ui->listView->setItemDelegate(delegate);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

     ui->listView->setFrameShape(QListView::NoFrame);
    //ui->listView->setGridSize(QSize(200, 240));

}



void LeftView::LoadGengeralStep()
{
    QStringList listComponents;
    listComponents << "代理模型" << "敏感性分析" << "优化" << "采样";

    QStringList listIcons;
    listIcons << MESH_IMAGE
              <<GEOMETRY_IMAGE
              <<OPTIMIZE_IMAGE
              <<SAMPLING_IMAGE;


    int type= StepData::MESH;  //不同组件的type 需要定义枚举
    for (int i=0; i<listComponents.size(); i++)
    {
        QStandardItem *pitem = new QStandardItem();
        pitem->setSizeHint(QSize(200,200));
        pitem->setText(listComponents.at(i));
        pitem->setIcon(QIcon(listIcons.at(i)));

        StepData data;
        data.m_text = listComponents.at(i);
        data.m_pix =  QPixmap(listIcons.at(i)).scaled(60,60);
        qDebug()<<data.m_pix.size();
        data.m_stepType = (StepData::STEP_TYPE)type;
        type++;
        pitem->setData(QVariant::fromValue(data),Qt::UserRole+1);
        gengeralModel->appendRow(pitem);
    }
}
