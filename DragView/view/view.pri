FORMS += \
    $$PWD/itemview.ui \
    $$PWD/leftview.ui \
    $$PWD/rightview.ui

HEADERS += \
    $$PWD/centreview.h \
    $$PWD/delegate.h \
    $$PWD/leftview.h \
    $$PWD/listview.h \
    $$PWD/rightview.h \
    $$PWD/thumbnail.h

SOURCES += \
    $$PWD/centreview.cpp \
    $$PWD/delegate.cpp \
    $$PWD/leftview.cpp \
    $$PWD/listview.cpp \
    $$PWD/rightview.cpp \
    $$PWD/thumbnail.cpp
