#ifndef SHADOWFRAME_H
#define SHADOWFRAME_H

#include <QFrame>

namespace Ui {
class ShadowFrame;
}

class ShadowFrame : public QFrame
{
    Q_OBJECT

public:
    explicit ShadowFrame(QWidget *parent = 0);
    ~ShadowFrame();

private:
    Ui::ShadowFrame *ui;
};

#endif // SHADOWFRAME_H
