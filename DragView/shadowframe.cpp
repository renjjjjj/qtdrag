#include "shadowframe.h"
#include "ui_shadowframe.h"

ShadowFrame::ShadowFrame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ShadowFrame)
{
    ui->setupUi(this);
}

ShadowFrame::~ShadowFrame()
{
    delete ui;
}
