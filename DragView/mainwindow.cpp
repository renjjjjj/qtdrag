#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QAction>
#include <QDockWidget>
#include <QToolBar>
#include "leftview.h"
#include "rightview.h"
#include "centreview.h"
#include "diagramscene.h"
#include "stepbase.h"
#include "propertydialog.h"
#include "shadowframe.h"
#include <QDebug>
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initWindow();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initWindow()
{
    initToolBar();
    initView();

    resize(1200,800);


}

void MainWindow::initToolBar()
{
    //创建菜单动作
    QAction *pResetAction = new QAction("重置(&R)", this);
    QAction *pDeleteAction = new QAction("删除(&D)", this);
    QAction *pSaveAction = new QAction("保存(&S)", this);
    QAction *pResultAction = new QAction("查看结果(&F)", this);

    pResetAction->setIcon(QIcon(":/images/reset.png"));
    pDeleteAction->setIcon(QIcon(":/images/deleteitem.png"));
    pSaveAction->setIcon(QIcon(":/images/save.png"));
    pResultAction->setIcon(QIcon(":/images/result.png"));

    pResetAction->setStatusTip("重置图形化界面");
    pDeleteAction->setStatusTip("删除一个item");
    pSaveAction->setStatusTip("保存图形化界面");
    pResultAction->setStatusTip("查看运行结果");

    QToolBar *pToolBar = new QToolBar(this);
    pToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    pToolBar->addAction(pResetAction);
    pToolBar->addAction(pDeleteAction);
    pToolBar->addAction(pSaveAction);
    pToolBar->addAction(pResultAction);
    this->addToolBar(pToolBar);

    connect(pResetAction, &QAction::triggered, this, &MainWindow::resetcentreView);
    connect(pDeleteAction, &QAction::triggered, this, &MainWindow::deleteItem);
    connect(pSaveAction, &QAction::triggered, this, &MainWindow::checkFlow);
}

void MainWindow::initView()
{
    //左窗口
    QDockWidget* dockLeft= new QDockWidget("dockLeft",this);
    QWidget* lTitleBar = dockLeft->titleBarWidget();
    QWidget* lEmptyWidget = new QWidget();
    dockLeft->setTitleBarWidget(lEmptyWidget);
    delete lTitleBar;

    LeftView* leftView = new LeftView;
    dockLeft->setWidget(leftView);
    addDockWidget(Qt::LeftDockWidgetArea,dockLeft);


    //右窗口
    QDockWidget* dockRight= new QDockWidget("dockRight",this);
    QWidget* rTitleBar = dockRight->titleBarWidget();
    QWidget* rEmptyWidget = new QWidget();
    dockRight->setTitleBarWidget(rEmptyWidget);
    delete rTitleBar;

    RightView* rightView = new RightView;
    dockRight->setWidget(rightView);
    addDockWidget(Qt::RightDockWidgetArea,dockRight);

    //主窗口
    CentreView* centerView = new CentreView(this);
    centerView->setCacheMode(QGraphicsView::CacheBackground);
    centerView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    DiagramScene* scene = new DiagramScene();
    currentScene = scene;
    centerView->setScene(scene);
    setCentralWidget(centerView);

    connect(scene,&DiagramScene::doubleClickedItem,this,&MainWindow::ShowDialog);

}

void MainWindow::deleteItem()
{
    QList<QGraphicsItem *> selectedItems = currentScene->selectedItems();
    if(!selectedItems.isEmpty())
    {
        StepBase* step = dynamic_cast<StepBase*>( selectedItems.first());
        if(step!=nullptr)
        {
            if(currentScene!=nullptr)
            {
                currentScene->removeItem(step);
                delete step;
                step = nullptr;
            }
        }
    }
}

void MainWindow::resetcentreView()
{
    if(currentScene)
    {
        currentScene->resetScene();
    }
}

void MainWindow::ShowDialog(StepBase *_step)
{
    if(_step!=nullptr)
    {
        PropertyDialog* dialog = new PropertyDialog;  //对话框要自定义
        showShadowImpl();
        int ret = dialog->exec();
        if(ret == QDialog::Accepted)
        {
            qDebug()<<"ddddddddddddd";
        }
        hideShadowImpl();
    }
}

void MainWindow::checkFlow()
{
    flowType = INVALID;
    if(currentScene!=nullptr)
    {
        QList<StepBase*> list;
        currentScene->ComponentOrdering(list);

        QList<StepData::STEP_TYPE> listType;
        for(auto _step:list)
        {
            qDebug()<<_step->data().m_stepType<<_step->data().m_text;
            listType.push_back(_step->data().m_stepType);
        }

        if(listType.size() == 3)
        {
            if(listType[0] == StepData::EXCHANGE &&
               listType[1] == StepData::STRUCTURE &&
               listType[2] == StepData::OPTIMIZE)
            {
                flowType = FLOW1;
            }
            else if(listType[0] == StepData::EXCHANGE &&
                    listType[1] == StepData::STRUCTURE &&
                    listType[2] == StepData::GEOMETRY)
             {
                flowType = FLOW2;
             }
        }
        else if(listType.size() == 4)
        {
            if(listType[0] == StepData::EXCHANGE &&
               listType[1] == StepData::STRUCTURE &&
               listType[2] == StepData::GEOMETRY &&
               listType[3] == StepData::MESH)
             {
                flowType = FLOW3;
             }
            else if(listType[0] == StepData::EXCHANGE &&
                    listType[1] == StepData::STRUCTURE &&
                    listType[2] == StepData::OPTIMIZE &&
                    listType[3] == StepData::MESH)
             {
                flowType = FLOW4;
             }
        }

    }
    if(flowType == INVALID)
    {
        QMessageBox::critical(this, "校验失败", "校验失败");
    }
    else
    {
        QMessageBox::information(this,"校验成功",QString("当前为流程%1").arg(flowType));
    }
}

void MainWindow::showShadowImpl()
{
    if(m_pShadowFrame == nullptr)
    {
        m_pShadowFrame = new ShadowFrame(this);
    }
    m_pShadowFrame->setFixedSize(size());
    m_pShadowFrame->show();
}

void MainWindow::hideShadowImpl()
{
    if(m_pShadowFrame)
    {
        m_pShadowFrame->hide();
    }
}


